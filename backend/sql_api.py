
import sqlite3
from sqlite3 import Error
from face_work import FaceWork
import func
import subprocess


img_face = 'temp_face'
img_mark = 'temp_mark'
txt_feature = 'temp_feature'
txt_gender = 'temp_gender'


class SQL_API:

    def __init__(self, db_name, table_name):
        """
            class initializer
            create db and table if not exist
        """
        self.db = db_name
        self.table = table_name
        self.db_src = 'db_image'
        self.max_id = 0
        self.face = FaceWork()
        self.face.mark_face("temp.jpg", img_mark, img_face, txt_feature, txt_gender)

        self.create_table()

    def create_table(self):
        """
            Create sql db and table. Table has such field:
                id(integer, key)      filename(string)        active(integer)       feature(string)
        """
        try:
            conn = sqlite3.connect(self.db)
            c = conn.cursor()

            sql = 'create table if not exists %s ' \
                  '(id integer PRIMARY KEY, ' \
                  'face_data text NOT NULL, ' \
                  'face_name text NOT NULL, ' \
                  'no_use1 text NOT NULL, ' \
                  'no_use2 integer, ' \
                  'face_feature text NOT NULL);' % self.table

            c.execute(sql)

            sql = 'select MAX(id) from %s' % self.table
            c.execute(sql)
            ret = c.fetchone()[0]

            if ret is None:
                self.max_id = 0
            else:
                self.max_id = ret

        except Error as e:
            print(e)

        finally:
            conn.close()

    def empty_table(self):
        """
            Delete all records in table
        """
        conn = sqlite3.connect(self.db)
        c = conn.cursor()
        c.execute('DROP TABLE %s' % self.table)
        conn.commit()
        self.max_id = 0
        self.create_table()

    def get_face_mark(self, image_file, src_type, image_face, image_mark, image_feautre, image_gender, db_data):

        if src_type == 'image-base64':
            face_cnt = self.face.mark_face(image_file, image_mark, image_face, image_feautre, image_gender, db_data)
        else:
            face_cnt = self.face.mark_face_video(image_file, image_mark, image_face, image_feautre, image_gender, db_data)
            # args = ['python', 'face_video.py', image_file, image_mark, image_face, image_feautre, image_gender]
            # proc = subprocess.Popen(args)
            # face_cnt = proc.wait()

        return face_cnt

    def create_face(self, img_file, src_type, face_name):
        """
            Insert new face image data into table
        """

        face_cnt = self.get_face_mark(img_file, src_type, img_face, img_mark, txt_feature, txt_gender, [])

        conn = sqlite3.connect(self.db)
        c = conn.cursor()

        ret = []

        for i in range(face_cnt):
            self.max_id += 1

            img_b64_data = func.img_encode_b64(img_face + str(i) + '.jpg')
            face_feature = func.read_text(txt_feature + str(i) + '.txt')
            age_gender_text = func.read_text(txt_gender + str(i) + '.txt')
            age_gender = age_gender_text.split()

            row = (self.max_id, img_b64_data, face_name, "", 0, face_feature)

            c.execute('insert into %s values (?,?,?,?,?,?)' % self.table, row)
            conn.commit()

            ret.append([str(self.max_id), face_name, age_gender[2], age_gender[3], age_gender[4], age_gender[5]])

        return ret

    def check_face(self, img_file, src_type):
        """
            Check the face
        """

        conn = sqlite3.connect(self.db)
        c = conn.cursor()
        c.execute('SELECT * FROM %s' % self.table)
        rows = c.fetchall()

        face_cnt = self.get_face_mark(img_file, src_type, img_face, img_mark, txt_feature, txt_gender, rows)
        ret = []

        for i in range(face_cnt):
            age_gender_text = func.read_text(txt_gender + str(i) + '.txt')
            face_info = age_gender_text.split()

            ret.append(face_info)

        return ret

    def delete_face(self, face_id):
        """
            Delete face_id record from table
        """
        try:
            conn = sqlite3.connect(self.db)
            c = conn.cursor()

            c.execute('SELECT * FROM %s WHERE id=%d' % (self.table, face_id))
            rows = c.fetchall()

            c.execute('DELETE FROM %s WHERE id = %d' % (self.table, face_id))
            conn.commit()

        except Error as e:
            print(e)
            return False, ''

        finally:
            if rows:
                return True, rows[0]
            else:
                return False, ''


if __name__ == '__main__':
    main_api = SQL_API('face_db.db', 'face_table')
    # print main_api.empty_table()
    # print main_api.create_face('../img_src/1.jpg', 'image-base64', 'Masha')
    print main_api.check_face('../img_src/1.jpg', 'image-base64')
    # print main_api.recog_face('1.jpg', '')
    # print main_api.delete_face(2)
    # print main_api.active_face(8)
    # print main_api.de_active_face(8)
