from flask import Flask, request, jsonify
from flask_restful import Resource, Api
import os
from sql_api import SQL_API
from datetime import datetime
import func
import shutil
import thread


img_face = 'temp_face'
img_mark = 'temp_mark'
txt_feature = 'temp_feature'
txt_gender = 'temp_gender'
img_name = ''


class face_api(Resource):

    def post(self):
        """ Post Function """

        """ ------------------------------------ Receiving Request Json ----------------------------------- """
        print "Request received."
        req_json = request.get_json()
        req_method = req_json['method']
        if 'type' in req_json:
            req_type = req_json['type']
        else:
            req_type = ''

        """ ----------------------------------------- check account ----------------------------------------"""
        result_json = {}
        result_json['method'] = req_method
        result_json['type'] = req_type

        if not account_list.__contains__(req_json['account']):
            result_json['state'] = 'Error'
            result_json['error'] = "This account doesn't allowed!"

            return jsonify(dict(result=result_json))

        """ ------------------------------------ Analyzing Request Json ------------------------------------ """
        if req_type == 'image-base64':
            img_name = 'temp' + str(datetime.now().microsecond) + '.jpg'
            func.img_decode_b64(req_json['data'], img_name)
        elif req_type == 'video-base64':
            img_name = 'temp' + str(datetime.now().microsecond) + '.mp4'
            func.img_decode_b64(req_json['data'], img_name)
        elif req_type == 'id':
            img_id = req_json['data']

        """ --------------------------------------- Processing Request ----------------------------------- """
        if req_method == 'enroll':
            ret_face = sql_class.create_face(img_name, req_type, req_json['name'])
        elif req_method == 'check':
            ret_face = sql_class.check_face(img_name, req_type)
        elif req_method == 'delete':
            ret, record = sql_class.delete_face(int(img_id))
        elif req_method == 'format':
            sql_class.empty_table()
            ret = True
        elif req_method == 'emotion_upload':
            shutil.copy(img_name, 'face_src/' + req_json['class'] + '/')
            ret = True
        elif req_method == 'emotion_train':
            os.system('python emotion_get_features.py')
            os.system('python emotion_training.py')
            ret = True
        else:
            ret = False

        """ --------------------------------------- Making Respond Json ----------------------------------- """
        if req_method == 'format' or req_method == 'emotion_train':
            result_json['state'] = 'Done'

        elif req_method == 'emotion_upload':
            result_json['state'] = 'Done'
            func.rm_file(img_name)

        elif req_method == 'download_db':
            result_json['state'] = 'Done'
            result_json['data'] = func.img_encode_b64('face_db.db')

        elif req_method == 'download_emotion_model':
            result_json['state'] = 'Done'
            result_json['data'] = func.img_encode_b64('models/emotion.pkl')

        elif req_method == 'enroll' or req_method == 'check':
            if req_method == 'check' and req_json['response'] == 'detail':
                if len(ret_face) > 0:
                    if req_type == 'image-base64':
                        result_json['data'] = func.img_encode_b64(img_mark + '.jpg')
                    elif req_type == 'video-base64':
                        result_json['data'] = func.img_encode_b64(img_mark + '.avi')

            result_source = []
            for i in range(len(ret_face)):
                result_item = {}

                result_item['id'] = ret_face[i][0]
                result_item['name'] = ret_face[i][1]
                result_item['age'] = ret_face[i][2]
                result_item['gender'] = ret_face[i][3]
                result_item['emotion'] = ret_face[i][4]

                if req_method == 'check':
                    result_item['match'] = ret_face[i][5]

                result_source.append(result_item)

                func.rm_file(img_face + str(i) + '.jpg')
                func.rm_file(txt_feature + str(i) + '.txt')
                func.rm_file(txt_gender + str(i) + '.txt')

            if len(ret_face) > 0:
                result_json['state'] = 'Done'
                result_json['result'] = result_source
            else:
                result_json['state'] = 'Error'
                result_json['error'] = "Face not detected!"

            func.rm_file(img_name)
            func.rm_file(img_mark + '.jpg')
            func.rm_file(img_mark + '.avi')

        else:
            if ret:
                result_json['state'] = 'Done'
            else:
                result_json['state'] = 'Error'
                result_json['error'] = 'Failed work!'

        return jsonify(dict(result=result_json))


app = Flask(__name__)
api = Api(app)
api.add_resource(face_api, '/api/face_server/v1.0')

sql_class = SQL_API('face_db.db', 'face_table')

account_list = func.load_csv('account_list.csv')[0]


if __name__ == '__main__':

    app.run(host="0.0.0.0",
            port=int(os.environ.get("PORT", 3000)),
            debug=False,
            threaded=False, )
