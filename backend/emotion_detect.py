
import func
import cPickle
import sys
import cv2
from face_work import FaceWork


if len(sys.argv) == 1:
    # img_name = 'face_src/angry/mine/ang1.jpg'
    img_name = '../img_src/10.jpg'
else:
    img_name = sys.argv[1]

# ------------------- load emotion list ---------------------
class_face = FaceWork()
emotion_list = func.load_csv('emotion_list.csv')[0]

# -------------------- load training data -------------------
img = cv2.imread(img_name)
landmark = class_face.get_landmarks(img)
feature = func.get_emotion_features(landmark)

with open('models/emotion.pkl', 'rb') as fid:
    new_clf = cPickle.load(fid)

pred_y = new_clf.predict([feature])
print pred_y[0], emotion_list[pred_y[0]]
