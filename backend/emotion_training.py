
import func
import os
from sklearn import svm
import cPickle


# ------------------- load emotion list ---------------------
emotion_list = func.load_csv('emotion_list.csv')[0]

# -------------------- load training data -------------------
print 'Loading data ...'
train_x = []
train_y = []
for i in range(len(emotion_list)):
    path_emotion = 'face_feature/' + emotion_list[i]
    for f_name in os.listdir(path_emotion):
        feature = func.load_csv(path_emotion + '/' + f_name)[0]
        train_x.append(feature)
        train_y.append(i)

print 'Training data ...'
clf = svm.SVC(kernel='linear', C=1.0)

clf.fit(train_x, train_y)
pred_y = clf.predict(train_x)

print "   accuracy:", float(len(pred_y & train_y))/len(train_y)

print "Saving model ..."
with open('models/emotion.pkl', 'wb') as fid:
    cPickle.dump(clf, fid)

# with open('models/emotion.pkl', 'rb') as fid:
#     new_clf = cPickle.load(fid)
