
import os
from face_work import FaceWork
import func
import cv2


class_face = FaceWork()
path_list, file_list, join_list = func.get_file_list('face_src')

if not os.path.isdir('face_feature'):
    os.mkdir('face_feature')

for i in range(len(file_list)):

    src_path = join_list[i].split('/')

    if len(src_path) <= 2:
        continue

    save_name = src_path[2]
    for j in range(3, len(src_path)):
        save_name += ('_' + src_path[j])

    if not os.path.isdir('face_feature/' + src_path[1]):
        os.mkdir('face_feature/' + src_path[1])

    save_full_name = 'face_feature/' + src_path[1] + '/' + save_name.replace('.', '_') + '.csv'

    if not os.path.isfile(save_full_name):
        img = cv2.imread(join_list[i])
        if img is None:
            continue

        landmark = class_face.get_landmarks(img)
        if landmark is None:
            continue

        feature = func.get_emotion_features(landmark[0])

        func.save_csv(save_full_name, [feature])
        print "Processing", str(i+1) + '/' + str(len(file_list)), join_list[i]
    else:
        print "Ignore", str(i + 1) + '/' + str(len(file_list)), join_list[i]
