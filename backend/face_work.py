
import dlib
import cv2
import numpy as np
import func
import os
from wide_resnet import WideResNet
import sqlite3
import cPickle


class FaceWork:

    def __init__(self):
        self.my_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(os.path.join(self.my_dir, 'models/dlib_model.dat'))
        self.recognizer = dlib.face_recognition_model_v1(
            os.path.join(self.my_dir, "models/dlib_face_recognition_resnet_model_v1.dat"))

        self.img_size = 64

        depth = 16
        k = 8

        self.emotion_list = func.load_csv('emotion_list.csv')[0]
        with open('models/emotion.pkl', 'rb') as fid:
            self.new_clf = cPickle.load(fid)

        self.model = WideResNet(self.img_size, depth=depth, k=k)()
        self.model.load_weights(os.path.join(self.my_dir, "models/weights.18-4.06.hdf5"))

    def mark_face(self, img_name, out_name_mark='', out_name_face='', feature_file='', gender_file='', db_data=[]):

        img = cv2.imread(img_name)
        if img is None:
            return 0

        # ----------------- get face feature and save it -----------------
        if feature_file != '':
            feature128_list = self.get_feature128(img)

            for f_ind in range(len(feature128_list)):
                text = ''
                for i in range(len(feature128_list[f_ind])):
                    text += str(feature128_list[f_ind][i]) + ' '
                func.write_text(feature_file + str(f_ind) + '.txt', text)

        # ------------------------- get faces -----------------------------
        ret = self.get_face(img)

        if not ret[0]:
            return 0

        # ----------------------- save face images -------------------------
        if out_name_face != '':
            for f_ind in range(len(ret[0])):
                img_face_resize = cv2.resize(ret[0][f_ind], (80, 75), interpolation=cv2.INTER_AREA)
                cv2.imwrite(out_name_face + str(f_ind) + '.jpg', img_face_resize)

        # ---------------- save id/name/age/gender/similarity --------------
        name_list = []
        str_emotion_list = []
        if gender_file != '':
            age_gender = self.detect_age_gender_multi_face(img)

            landmark = self.get_landmarks(img)

            for f_ind in range(len(age_gender)):
                if len(db_data) == 0:
                    str_id = "0"
                    str_name = "fake"
                    str_match = "100.0"
                else:

                    max_match = 0.0
                    for row in db_data:
                        feature_row = row[5].split()

                        match_val = func.get_match_value(feature128_list[f_ind], feature_row)

                        if match_val > max_match:
                            max_match = match_val
                            max_row = row

                    str_id = str(max_row[0])
                    str_name = max_row[2]
                    str_match = str(max_match)

                name_list.append(str_name)

                feature = func.get_emotion_features(landmark[f_ind])
                pred_y = self.new_clf.predict([feature])
                str_emotion = self.emotion_list[pred_y[0]]

                str_emotion_list.append(str_emotion)

                func.write_text(gender_file + str(f_ind) + '.txt',
                                str_id + ' ' + str_name + ' ' + str(age_gender[f_ind][0]) + ' ' +
                                age_gender[f_ind][1] + ' ' + str_emotion + ' ' + str_match)

        # ------------------------ save mark image -------------------------
        if out_name_mark != '' and len(db_data) > 0:

            for f_ind in range(len(ret[0])):

                cv2.rectangle(img, (ret[1][f_ind][0], ret[1][f_ind][2]), (ret[1][f_ind][1], ret[1][f_ind][3]),
                              (0, 255, 0), 2)
                cv2.putText(img, name_list[f_ind], (ret[1][f_ind][0], ret[1][f_ind][3] + 24),
                            cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)
                cv2.putText(img, age_gender[f_ind][1][0] + ', ' + str(age_gender[f_ind][0]),
                            (ret[1][f_ind][0], ret[1][f_ind][3]+48),
                            cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)
                cv2.putText(img, str_emotion_list[f_ind], (ret[1][f_ind][0], ret[1][f_ind][3] + 72),
                            cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)

            cv2.imwrite(out_name_mark + '.jpg', img)

        return len(ret[0])

    def mark_face_video(self, video_name, out_name_mark='', out_name_face='', feature_file='', gender_file='',
                        db_data=[], show_video=False):

        cap = cv2.VideoCapture(video_name)
        fource = cv2.VideoWriter_fourcc(*"XVID")

        frame_cnt = -1
        capture_step = 3
        face_rec_list = []
        feature_rec_list = []
        str_emotion = 'neutral'

        while True:
            ret, img = cap.read()
            frame_cnt += 1

            if img is None:
                break

            if frame_cnt == 0:
                video_writer = cv2.VideoWriter(out_name_mark + '.avi', fource, 24.0,
                                               (img.shape[:2][1], img.shape[:2][0]))

            # ----------------- get face feature and save it -----------------
            if frame_cnt % capture_step == 0:
                face_list = []
                coordinate_list = []
                feature = []
                rect = self.__get_face_rect(img)

                for i, d in enumerate(rect):
                    x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
                    im_face = img[y1:y2, x1:x2]
                    face_list.append(im_face)
                    coordinate_list.append([x1, x2, y1, y2])

                    face_description = self._recognize_face(img, d)
                    feature.append(np.asarray(face_description))

                if not face_list:
                    continue

                # ---------------- save id/name/age/gender/similarity --------------

                name_list = []
                age_list = []
                for f_ind in range(len(face_list)):
                    max_match = 0.0
                    for row in db_data:
                        feature_row = row[5].split()
                        match_val = func.get_match_value(feature[f_ind], feature_row)

                        if match_val > max_match:
                            max_match = match_val
                            max_row = row

                    name_list.append(max_row[2])

                    if frame_cnt % (capture_step * 2) == 0:
                        # age_gender = self.detect_age_gender_single_face(face_list[f_ind])

                        if len(face_rec_list) == 0:
                            feature_rec_list.append(feature[f_ind])
                            face_rec_list.append(face_list[f_ind])
                        else:
                            f_dup = False
                            for i in range(len(face_rec_list)):
                                match_val = func.get_match_value(feature[f_ind], feature_rec_list[i])
                                if match_val > 95.0:
                                    f_dup = True
                                    break

                            if not f_dup:
                                feature_rec_list.append(feature[f_ind])
                                face_rec_list.append(face_list[f_ind])

                    # age_list.append(age_gender[0][1][0] + ', ' + str(age_gender[0][0]))

            for f_ind in range(len(face_list)):
                cv2.rectangle(img, (coordinate_list[f_ind][0], coordinate_list[f_ind][2]),
                              (coordinate_list[f_ind][1], coordinate_list[f_ind][3]), (0, 255, 0), 2)
                cv2.putText(img, name_list[f_ind], (coordinate_list[f_ind][0], coordinate_list[f_ind][3] + 24),
                            cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)
                # cv2.putText(img, age_list[f_ind],
                #             (coordinate_list[f_ind][0], coordinate_list[f_ind][3] + 48),
                #             cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)
                # cv2.putText(img, "smile", (ret[1][f_ind][0], ret[1][f_ind][3] + 72),
                #             cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 255), 1)

            video_writer.write(img)

            if show_video:
                cv2.imshow("qq", img)
                k = cv2.waitKey(20) & 0xff
                if k == 27:
                    break

        cap.release()
        video_writer.release()

        for f_ind in range(len(feature_rec_list)):
            if out_name_face != '':
                img_face_resize = cv2.resize(face_rec_list[f_ind], (80, 75), interpolation=cv2.INTER_AREA)
                cv2.imwrite(out_name_face + str(f_ind) + '.jpg', img_face_resize)

            if feature_file != '':
                text = ''
                for i in range(len(feature_rec_list[f_ind])):
                    text += str(feature_rec_list[f_ind][i]) + ' '
                func.write_text(feature_file + str(f_ind) + '.txt', text)

            if gender_file != '':
                age_gender = self.detect_age_gender_single_face(face_rec_list[f_ind])

                max_match = 0.0
                for row in db_data:
                    feature_row = row[5].split()
                    match_val = func.get_match_value(feature_rec_list[f_ind], feature_row)

                    if match_val > max_match:
                        max_match = match_val
                        max_row = row

                str_id = str(max_row[0])
                str_name = max_row[2]
                str_match = str(max_match)

                landmark = self.get_landmarks(face_rec_list[f_ind])
                if landmark is not None:
                    feature = func.get_emotion_features(landmark[0])
                    pred_y = self.new_clf.predict([feature])
                    str_emotion = self.emotion_list[pred_y[0]]

                func.write_text(gender_file + str(f_ind) + '.txt',
                                str_id + ' ' + str_name + ' ' + str(age_gender[0][0]) + ' ' +
                                age_gender[0][1] + ' ' + str_emotion + ' ' + str_match)

        return len(face_rec_list)

    def get_landmarks(self, im):
        rects = self.__get_face_rect(im)
        points_list = []

        if len(rects) != 0:
            for i in range(len(rects)):
                predict_ret = self.predictor(im, rects[i]).parts()
                points = []
                for p in predict_ret:
                    points.append((p.x, p.y))

                points_list.append(points)

            return points_list

        else:
            return None

    def __get_face_rect(self, im):
        rect = self.detector(im, 0)
        return rect

    def get_face(self, im):
        face_list = []
        coordinate_list = []
        rect = self.__get_face_rect(im)

        for i, d in enumerate(rect):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            im_face = im[y1:y2, x1:x2]
            face_list.append(im_face)
            coordinate_list.append([x1, x2, y1, y2])

        return face_list, coordinate_list

    def detect_age_gender_multi_face(self, im):

        face_list, face_rect_list = self.get_face(im)

        if len(face_list) == 0:
            return None

        img_h, img_w, _ = np.shape(im)

        faces = np.empty((len(face_rect_list), self.img_size, self.img_size, 3))

        for i in range(len(face_rect_list)):
            x1, x2, y1, y2 = face_rect_list[i]
            w = x2 - x1
            h = y2 - y1
            xw1 = max(int(x1 - 0.4 * w), 0)
            yw1 = max(int(y1 - 0.4 * h), 0)
            xw2 = min(int(x2 + 0.4 * w), img_w - 1)
            yw2 = min(int(y2 + 0.4 * h), img_h - 1)

            faces[i, :, :, :] = cv2.resize(im[yw1:yw2 + 1, xw1:xw2 + 1, :], (self.img_size, self.img_size))

        results = self.model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

        ret_age_gender = []
        for i in range(len(face_rect_list)):
            ret_age_gender.append([int(predicted_ages[i]),
                                   "Female" if predicted_genders[i][0] > 0.5 else "Male"])

        return ret_age_gender

    def detect_age_gender_single_face(self, im):

        faces = np.empty((1, self.img_size, self.img_size, 3))

        faces[0, :, :, :] = cv2.resize(im, (self.img_size, self.img_size))

        results = self.model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

        ret_age_gender = [[int(predicted_ages[0]), "Female" if predicted_genders[0][0] > 0.5 else "Male"]]

        return ret_age_gender

    def _recognize_face(self, frame, rect):
        shape = self.predictor(frame, rect)
        face_description = self.recognizer.compute_face_descriptor(frame, shape)
        return np.asarray(face_description)

    def get_feature128(self, frame):

        dets = self.__get_face_rect(frame)

        descriptions = []

        for k, r in enumerate(dets):
            face_description = self._recognize_face(frame, r)
            descriptions.append(np.asarray(face_description))

        return descriptions


if __name__ == '__main__':
    class_face = FaceWork()
    # img_name = '../img_src/w1.jpg'
    # print class_face.mark_face(img_name, 'temp_mark', 'temp_face', 'temp_feature', 'temp_gender')

    # conn = sqlite3.connect('face_db.db')
    # c = conn.cursor()
    # c.execute('SELECT * FROM face_table')
    # db_data = c.fetchall()

    # video_name = '../video_src/bbc1.mp4'
    # video_name = '../video_src/clips.mov'
    # class_face.mark_face_video(video_name, 'temp_mark', 'temp_face', 'temp_feature', 'temp_gender', db_data, True)

    # image = cv2.imread("../1.jpg")
    # print class_face.get_feature128(image)

    # image = cv2.imread("../1.jpg")
    # f1 = class_face.get_feature128(image)

    # file1 = open("temp.txt", 'w')
    # for i in range(len(f1[0])):
    #     file1.writelines(str(f1[0][i]))
    # file1.close()

    # print class_face.mark_face('../5.jpg')
    img = cv2.imread('1.jpg')
    if img is not None:
        print class_face.detect_age_gender_multi_face(img)
