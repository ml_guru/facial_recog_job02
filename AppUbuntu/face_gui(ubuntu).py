
from kivy.app import App
from kivy.config import Config
from kivy.lang import Builder
from kivy.properties import ListProperty, StringProperty
from kivy.clock import Clock
from kivy.graphics.texture import Texture
import base64
import requests
import json
import time
import thread
from face_engine import FaceEngine
import sqlite3
from yolo_tiny import *
import func
import os


yolo = yolo_tf()


class BestApp(App):

    screen_names = ListProperty([])
    screens = {}  # Dict of all screens

    title = StringProperty()
    txt_file_name = StringProperty()
    txt_cam_setting = StringProperty()
    url_server = StringProperty()
    account_id = StringProperty()

    rec_time0 = StringProperty()
    rec_time1 = StringProperty()
    rec_time2 = StringProperty()
    rec_time3 = StringProperty()
    rec_time4 = StringProperty()
    rec_time5 = StringProperty()
    rec_time6 = StringProperty()
    rec_time7 = StringProperty()
    rec_time8 = StringProperty()
    rec_time9 = StringProperty()
    rec_id0 = StringProperty()
    rec_id1 = StringProperty()
    rec_id2 = StringProperty()
    rec_id3 = StringProperty()
    rec_id4 = StringProperty()
    rec_id5 = StringProperty()
    rec_id6 = StringProperty()
    rec_id7 = StringProperty()
    rec_id8 = StringProperty()
    rec_id9 = StringProperty()
    rec_name0 = StringProperty()
    rec_name1 = StringProperty()
    rec_name2 = StringProperty()
    rec_name3 = StringProperty()
    rec_name4 = StringProperty()
    rec_name5 = StringProperty()
    rec_name6 = StringProperty()
    rec_name7 = StringProperty()
    rec_name8 = StringProperty()
    rec_name9 = StringProperty()
    rec_match0 = StringProperty()
    rec_match1 = StringProperty()
    rec_match2 = StringProperty()
    rec_match3 = StringProperty()
    rec_match4 = StringProperty()
    rec_match5 = StringProperty()
    rec_match6 = StringProperty()
    rec_match7 = StringProperty()
    rec_match8 = StringProperty()
    rec_match9 = StringProperty()
    rec_file0 = StringProperty()
    rec_file1 = StringProperty()
    rec_file2 = StringProperty()
    rec_file3 = StringProperty()
    rec_file4 = StringProperty()
    rec_file5 = StringProperty()
    rec_file6 = StringProperty()
    rec_file7 = StringProperty()
    rec_file8 = StringProperty()
    rec_file9 = StringProperty()
    rec_photo0 = StringProperty()
    rec_photo1 = StringProperty()
    rec_photo2 = StringProperty()
    rec_photo3 = StringProperty()
    rec_photo4 = StringProperty()
    rec_photo5 = StringProperty()
    rec_photo6 = StringProperty()
    rec_photo7 = StringProperty()
    rec_photo8 = StringProperty()
    rec_photo9 = StringProperty()
    txt_emotion1 = StringProperty()
    txt_emotion2 = StringProperty()
    txt_emotion3 = StringProperty()
    txt_emotion4 = StringProperty()
    txt_emotion5 = StringProperty()
    txt_emotion6 = StringProperty()
    txt_emotion7 = StringProperty()
    txt_emotion8 = StringProperty()
    img_emotion = StringProperty()

    def __init__(self, **kwargs):

        self.face_attendance = func.load_csv('attendance.csv')
        self.class_face = FaceEngine()
        self.run_mode = True
        self.record_mode = True
        self.event_take_video = None

        self.title = 'Face Recognition'
        self.dlg_ind = -1
        self.url_server = 'http://18.217.112.30:3000/api/face_server/v1.0'
        self.account_id = 'account1'
        self.dlg_list = []
        self.coordinate_list = []
        self.name_list = []
        self.face_list = []
        self.str_emotion_list = []
        self.dlg_para = ''
        self.frame = None

        self.f_recog = True
        self.f_age_gender = False
        self.f_emotion = False
        self.f_realtime = False

        self.fps = 60
        self.cam_ind = 0
        self.fram_ind = 0
        self.rec_pos = 0
        self.sel_emotion_ind = 0

        self.red = (0, 0, 255)
        self.blue = (255, 0, 0)
        self.white = (255, 255, 255)

        self.emotion_list = func.load_csv('emotion_list.csv')[0]
        self.txt_emotion1 = self.emotion_list[0]
        self.txt_emotion2 = self.emotion_list[1]
        self.txt_emotion3 = self.emotion_list[2]
        self.txt_emotion4 = self.emotion_list[3]
        self.txt_emotion5 = self.emotion_list[4]
        self.txt_emotion6 = self.emotion_list[5]
        self.txt_emotion7 = self.emotion_list[6]
        if len(self.emotion_list) > 7:
            self.txt_emotion8 = self.emotion_list[7]
        else:
            self.txt_emotion8 = 'New'

        self.cam_src = ['0',
                        'rtsp://admin:admin@192.168.2.188:554/cam/realmonitor?channel=1&subtype=0',
                        'Sample_video/sample1.mp4',
                        'sample_Image/image2.jpg',
                        'logo/img_logo.jpg']

        thread.start_new_thread(self.thread_load, ('thread', self.cam_src[4]))

        super(BestApp, self).__init__(**kwargs)

    """ --------------------------------- Main Menu Event -------------------------------- """
    def on_button_home(self, index, para=''):
        self.dlg_ind = index
        self.txt_file_name = ''
        self.dlg_para = para

        if index == 0:
            self.title = 'Enroll Image to Server'
            self.go_screen('dlg_enroll', 'left')
            thread.start_new_thread(self.thread_load, ('thread', self.cam_src[self.cam_ind]))

        elif index == 1:
            self.title = 'Check Image to Server'
            self.go_screen('dlg_check', 'left')

        elif index == 2:
            self.title = 'Check Video to Server'
            self.go_screen('dlg_check', 'left')

        elif index == 3:
            self.title = 'Real-time Face Recognition'
            self.go_screen('dlg_realtime', 'left')
            self.f_realtime = False

            conn = sqlite3.connect('face_db.db')
            c = conn.cursor()
            c.execute('SELECT * FROM face_table')
            self.db_face = c.fetchall()

            thread.start_new_thread(self.thread_load, ('thread', self.cam_src[self.cam_ind]))

        elif index == 4:
            self.title = 'Real-time Object Detection'
            self.go_screen('dlg_yolo', 'left')
            thread.start_new_thread(self.thread_load, ('thread', self.cam_src[self.cam_ind]))

        elif index == 5:
            self.title = 'Emotion Training'
            self.go_screen('dlg_emotion_training', 'left')

        elif index == 6:
            self.on_stop()
            self.title = 'Setting'
            self.txt_cam_setting = self.cam_src[self.cam_ind]
            self.go_screen('dlg_setting', 'left')

        elif index == 7:
            self.capture.release()
            exit(0)

    def on_return(self):
        if self.dlg_para == 'enroll':
            self.dlg_ind = 0
            self.title = 'Enroll Image'
            self.go_screen('dlg_enroll', 'right')
            self.on_resume()
            # thread.start_new_thread(self.thread_load, ('thread', self.cam_src[self.cam_ind]))
        else:
            self.title = 'Face Recognition'
            self.go_screen('dlg_home', 'right')
            self.on_stop()

            if self.dlg_ind == 3:
                func.save_csv('attendance.csv', self.face_attendance)

            thread.start_new_thread(self.thread_load, ('thread', self.cam_src[4]))

    def thread_load(self, thread_name, cam_set):
        time.sleep(0.4)
        self.cam_setting = cam_set
        self._init_cv()
        self.on_resume()

    """ ------------------------------ Enroll and Check dialog Event ----------------------------- """
    def on_enroll(self, face_name, file_name=''):
        print "1"
        try:
            if self.dlg_ind == 0:
                file_name = 'temp.jpg'
                cv2.imwrite(file_name, self.frame)

            data_file = open(file_name, 'rb')
        except:
            return None

        if self.dlg_ind == 0:       # enroll
            json_data = {
                "method": "enroll",
                "type": "image-base64",
                "data": base64.b64encode(data_file.read()).decode('UTF-8'),
                "name": face_name,
                "account": self.account_id,
                "response": 'detail'
            }
        elif self.dlg_ind == 1:     # check image
            json_data = {
                "method": "check",
                "type": "image-base64",
                "data": base64.b64encode(data_file.read()).decode('UTF-8'),
                "account": self.account_id,
                "response": 'detail'
            }
        elif self.dlg_ind == 2:  # check Video
            json_data = {
                "method": "check",
                "type": "video-base64",
                "data": base64.b64encode(data_file.read()).decode('UTF-8'),
                "account": self.account_id,
                "response": 'detail'
            }
        else:
            return None

        print json_data
        response = requests.post(url=self.url_server, json=json_data)
        print("Server responded with %s" % response.status_code)

        rep_json = response.json()['result']
        print json.dumps(rep_json, indent=4)

        if rep_json['state'] == 'Done' and 'data' in rep_json:
            if self.dlg_ind == 0 or self.dlg_ind == 1:
                data_out = 'reply_image.jpg'
            else:
                data_out = 'reply_video.avi'

            img_out = open(data_out, 'wb')
            img_out.write(rep_json['data'].decode('base64'))
            img_out.close()

            thread.start_new_thread(self.thread_load, ('thread', data_out))

    def on_file_open(self):
        self.title = 'File Open'
        self.go_screen('dlg_file', 'left')

    def on_file_select(self, state, file_name):

        if self.dlg_ind == 6:
            self.title = 'Setting'
            self.go_screen('dlg_setting', 'right')

            if state == 'sel':
                self.txt_cam_setting = file_name[0]

        elif self.dlg_ind == 5:
            self.title = 'Emotion Training'
            self.go_screen('dlg_emotion_training', 'right')

            if state == 'sel':
                self.img_emotion = file_name[0]

        else:
            if state == 'sel':
                self.txt_file_name = file_name[0]
                thread.start_new_thread(self.thread_load, ('thread', file_name[0]))
                # self.root.ids.img_video.source = file_name[0]

            if self.dlg_ind == 1:
                self.title = 'Check Image to Server'
                self.go_screen('dlg_check', 'right')
            elif self.dlg_ind == 2:
                self.title = 'Check Video to Server'
                self.go_screen('dlg_check', 'right')

    """ ---------------------------- Emotion Training Event ------------------------------- """
    def on_sel_emotion(self, sel_ind):
        self.sel_emotion_ind = sel_ind

    def on_emotion_upload(self, image_name):
        if self.sel_emotion_ind < len(self.emotion_list):

            if os.path.isfile(image_name):
                emotion_class = self.emotion_list[self.sel_emotion_ind]
                data_file = open(image_name, 'rb')

                json_data = {
                    "account": self.account_id,
                    "method": "emotion_upload",
                    "type": "image-base64",
                    "class": emotion_class,
                    "data": base64.b64encode(data_file.read()).decode('UTF-8')
                }

                response = requests.post(url=self.url_server, json=json_data)
                print("Server responded with %s" % response.status_code)

                rep_json = response.json()['result']
                print json.dumps(rep_json, indent=4)
            else:
                print "File not exist"
        else:
            print "Class not selected."

    def on_emotion_train(self):
        json_data = {
            "account": self.account_id,
            "method": "emotion_train"
        }

        response = requests.post(url=self.url_server, json=json_data)
        print("Server responded with %s" % response.status_code)

        rep_json = response.json()['result']
        print json.dumps(rep_json, indent=4)

    """ ---------------------------- View Record Dialog Event ------------------------------- """
    def on_view_record(self):
        self.on_rec_move(3)
        self.title = 'View Record Data'
        self.go_screen('dlg_view_record', 'up')

    def on_return_rec(self):
        self.title = 'Real-time Face Recognition'
        self.go_screen('dlg_realtime', 'down')

    def on_rec_move(self, index):
        if index == 0:
            self.rec_pos = 0
        elif index == 1:
            self.rec_pos = max(self.rec_pos - 10, 0)
        elif index == 2:
            self.rec_pos = max(min(self.rec_pos + 10, len(self.face_attendance) - 10), 0)
        elif index == 3:
            self.rec_pos = max(len(self.face_attendance) - 10, 0)

        self.rec_time0, self.rec_id0, self.rec_name0, self.rec_match0, self.rec_file0, self.rec_photo0 = self.get_rec_data(self.rec_pos)
        self.rec_time1, self.rec_id1, self.rec_name1, self.rec_match1, self.rec_file1, self.rec_photo1 = self.get_rec_data(self.rec_pos+1)
        self.rec_time2, self.rec_id2, self.rec_name2, self.rec_match2, self.rec_file2, self.rec_photo2 = self.get_rec_data(self.rec_pos+2)
        self.rec_time3, self.rec_id3, self.rec_name3, self.rec_match3, self.rec_file3, self.rec_photo3 = self.get_rec_data(self.rec_pos+3)
        self.rec_time4, self.rec_id4, self.rec_name4, self.rec_match4, self.rec_file4, self.rec_photo4 = self.get_rec_data(self.rec_pos+4)
        self.rec_time5, self.rec_id5, self.rec_name5, self.rec_match5, self.rec_file5, self.rec_photo5 = self.get_rec_data(self.rec_pos+5)
        self.rec_time6, self.rec_id6, self.rec_name6, self.rec_match6, self.rec_file6, self.rec_photo6 = self.get_rec_data(self.rec_pos+6)
        self.rec_time7, self.rec_id7, self.rec_name7, self.rec_match7, self.rec_file7, self.rec_photo7 = self.get_rec_data(self.rec_pos+7)
        self.rec_time8, self.rec_id8, self.rec_name8, self.rec_match8, self.rec_file8, self.rec_photo8 = self.get_rec_data(self.rec_pos+8)
        self.rec_time9, self.rec_id9, self.rec_name9, self.rec_match9, self.rec_file9, self.rec_photo9 = self.get_rec_data(self.rec_pos+9)

    def get_rec_data(self, pos):
        if len(self.face_attendance) <= pos:
            return '', '', '', '', '', 'logo/empty.jpg'
        else:
            return self.conv_date_time(self.face_attendance[pos][0]), self.face_attendance[pos][1], \
                   self.face_attendance[pos][2], self.face_attendance[pos][3], self.face_attendance[pos][4][9:], \
                   self.face_attendance[pos][4]

    def conv_date_time(self, str_time_stamp):
        return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(int(str_time_stamp)))

    """ ----------------------------- Setting dialog Event --------------------------- """
    def on_sel_cam(self, cam_sel):
        # self.cam_ind = cam_sel
        self.txt_cam_setting = self.cam_src[cam_sel]

    def on_cam_set(self, cam_setting, txt_server, txt_account, check1, check2, check3, toggle1, toggle2, toggle3):

        self.f_recog = (check1 == 'down')
        self.f_age_gender = (check2 == 'down')
        self.f_emotion = (check3 == 'down')

        if toggle1 == 'down':
            self.cam_ind = 0
        elif toggle2 == 'down':
            self.cam_ind = 1
        elif toggle3 == 'down':
            self.cam_ind = 2
        else:
            self.cam_ind = 3

        self.cam_src[self.cam_ind] = cam_setting
        self.cam_setting = cam_setting

        self.url_server = txt_server
        self.account_id = txt_account

        # self._init_cv()
        # self.on_resume()

        if self.dlg_para == 'enroll':
            self.dlg_ind = 0
            thread.start_new_thread(self.thread_load, ('thread', self.cam_src[self.cam_ind]))
            self.title = 'Enroll Image'
            self.go_screen('dlg_enroll', 'right')
        else:
            self.title = 'Face Recognition'
            self.go_screen('dlg_home', 'right')

    def on_download_db(self, down_ind):

        if down_ind == 0:
            json_method = "download_db"
            data_out = 'face_db.db'
        elif down_ind == 1:
            json_method = "download_emotion_model"
            data_out = 'models/emotion.pkl'

        json_data = {
            "account": self.account_id,
            "method": json_method
        }

        response = requests.post(url=self.url_server, json=json_data)
        print("Server responded with %s" % response.status_code)

        rep_json = response.json()['result']
        print json.dumps(rep_json, indent=4)

        if rep_json['state'] == 'Done' and 'data' in rep_json:
            img_out = open(data_out, 'wb')
            img_out.write(rep_json['data'].decode('base64'))
            img_out.close()

    """ --------------------------------- Image Processing ----------------------------------- """
    def _init_cv(self):
        if self.cam_setting == '0':
            self.capture = cv2.VideoCapture(0)
        else:
            self.capture = cv2.VideoCapture(self.cam_setting)

        if not self.capture.isOpened():
            print('Failed to get source of CV')
        else:
            self.frameWidth = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.frameHeight = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
            # self.fps = self.capture.get(cv2.CV_CAP_PROP_FPS)

    def thread_frame_realtime(self, thread, frame):
        self.face_list, self.coordinate_list, self.name_list, self.age_gender, self.str_emotion_list = \
            self.class_face.check_image(frame,
                                        self.db_face,
                                        f_recog=self.f_recog,
                                        f_age_gender=self.f_age_gender,
                                        f_emotion=self.f_emotion)

        self.f_realtime = False

    def get_frame(self, *args):
        ret, frame = self.capture.read()

        if ret:
            self.fram_ind += 1
            self.frame = frame

            if self.dlg_ind == 3:

                self.thread_frame_realtime('', frame)

                # if self.f_realtime == False:
                #
                #     self.f_realtime = True
                #     thread.start_new_thread(self.thread_frame_realtime, ('thread', frame))

                list_co = self.coordinate_list
                list_name = self.name_list
                list_face = self.face_list
                list_emotion = self.str_emotion_list

                for i in range(len(list_co)):
                    frame = cv2.rectangle(frame,
                                          (list_co[i][0], list_co[i][2]),
                                          (list_co[i][1], list_co[i][3]),
                                          self.red, 2)

                    label_data = []
                    if len(list_name) > 0:
                        if float(list_name[i][2]) > 95.0:
                            label_data.append(list_name[i][1])

                            # ------------- save face data ----------------
                            time_stamp = str(int(time.time()))
                            face_id = list_name[i][0]
                            face_name = list_name[i][1]
                            face_match = '{:05.2f}'.format(float(list_name[i][2]))

                            f_dup = False
                            for j in range(len(self.face_attendance)):
                                time_delay = int(time_stamp) - int(self.face_attendance[j][0])
                                if self.face_attendance[j][1] == face_id and time_delay < 60:
                                    f_dup = True
                                    break

                            if not f_dup:
                                if not os.path.isdir('face_rec'):
                                    os.mkdir('face_rec')

                                face_out = 'face_rec/' + str(time_stamp) + '_' + str(face_id) + '.jpg'
                                cv2.imwrite(face_out, list_face[i])
                                self.face_attendance.append([time_stamp, face_id, face_name, face_match, face_out])

                        else:
                            label_data.append('Unknown')

                    if len(self.age_gender) > 0:
                        label_data.append(self.age_gender[i][1] + ', ' + str(self.age_gender[i][0]))

                    if len(list_emotion) > 0:
                        label_data.append(list_emotion[i])

                    if len(label_data) > 0:
                        pos_x = list_co[i][0]
                        pos_y = list_co[i][3]
                        frame = cv2.rectangle(frame, (pos_x - 1, pos_y - 1),
                                              (pos_x + 101, pos_y + 15 * len(label_data) + 4), self.blue, -1)
                        frame = cv2.rectangle(frame, (pos_x, pos_y), (pos_x + 100, pos_y + 15 * len(label_data) + 3),
                                              self.white, -1)
                        for j in range(len(label_data)):
                            cv2.putText(frame, label_data[j], (pos_x + 2, pos_y + j * 14 + 14), cv2.FONT_HERSHEY_PLAIN,
                                        1, self.blue, 1)

            elif self.dlg_ind == 4:
                detect_from_file(yolo, frame)
                frame = show_results(frame, yolo)

            self.frame_to_buf(frame=frame)
        else:
            self.root.ids.img_video.source = 'logo/img_logo.jpg'
            self.event_take_video.cancel()
            Clock.unschedule(self.event_take_video)
            self.event_take_video = None

    def frame_to_buf(self, frame):
        frame = cv2.resize(frame, (self.frameWidth, self.frameHeight))
        buf1 = cv2.flip(frame, 0)
        buf = buf1.tostring()
        self.root.ids.img_video.texture = Texture.create(size=(self.frameWidth, self.frameHeight))
        self.root.ids.img_video.texture.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')

    """ --------------------------------- Main Control ----------------------------------- """
    def on_resume(self):
        if self.event_take_video is None:
            self.event_take_video = Clock.schedule_interval(self.get_frame, 1.0 / self.fps)
        elif not self.event_take_video.is_triggered:
            self.event_take_video()

    def on_stop(self):
        if self.event_take_video is not None and self.event_take_video.is_triggered:
            self.event_take_video.cancel()

    def build(self):
        self.load_screen()
        self.go_screen('dlg_home', 'right')

    def go_screen(self, dest_screen, direction):
        sm = self.root.ids.sm
        sm.switch_to(self.screens[dest_screen], direction=direction)

    def load_screen(self):
        self.screen_names = ['dlg_home', 'dlg_enroll', 'dlg_check', 'dlg_setting', 'dlg_file', 'dlg_realtime',
                             'dlg_yolo', 'dlg_view_record', 'dlg_emotion_training']

        for i in range(len(self.screen_names)):
            screen = Builder.load_file('kv_dlg/' + self.screen_names[i] + '.kv')
            self.screens[self.screen_names[i]] = screen
        return True


if __name__ == '__main__':
    Config.set('graphics', 'width', '1000')
    Config.set('graphics', 'height', '700')
    Config.set('graphics', 'resizable', 0)
    Config.set('kivy', 'window_icon', 'logo/logo_5Zc_icon.ico')
    BestApp().run()
