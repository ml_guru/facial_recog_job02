
import dlib
import cv2
import numpy as np
import func
import os
from wide_resnet import WideResNet
import sqlite3
import cPickle
import time


class FaceEngine:

    def __init__(self):
        self.my_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

        self.detector = dlib.get_frontal_face_detector()
        self.predictor = dlib.shape_predictor(os.path.join(self.my_dir, 'models/dlib_model.dat'))
        self.recognizer = dlib.face_recognition_model_v1(
            os.path.join(self.my_dir, "models/dlib_face_recognition_resnet_model_v1.dat"))

        self.img_size = 64

        depth = 16
        k = 8

        self.emotion_list = func.load_csv('emotion_list.csv')[0]
        with open('models/emotion.pkl', 'rb') as fid:
            self.new_clf = cPickle.load(fid)

        self.model = WideResNet(self.img_size, depth=depth, k=k)()
        self.model.load_weights(os.path.join(self.my_dir, "models/weights.18-4.06.hdf5"))

    def get_landmarks(self, im, rects):
        points_list = []

        if len(rects) != 0:
            for i in range(len(rects)):
                predict_ret = self.predictor(im, rects[i]).parts()
                points = []
                for p in predict_ret:
                    points.append((p.x, p.y))

                points_list.append(points)

            return points_list

        else:
            return None

    def __get_face_rect(self, im):
        rect = self.detector(im, 0)
        return rect

    def get_face(self, im):
        face_list = []
        coordinate_list = []
        rect = self.__get_face_rect(im)

        for i, d in enumerate(rect):
            x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()
            im_face = im[y1:y2, x1:x2]
            face_list.append(im_face)
            coordinate_list.append([x1, x2, y1, y2])

        return rect, face_list, coordinate_list

    def detect_age_gender_multi_face(self, im, face_list, face_rect_list):

        if len(face_list) == 0:
            return None

        img_h, img_w, _ = np.shape(im)

        faces = np.empty((len(face_rect_list), self.img_size, self.img_size, 3))

        for i in range(len(face_rect_list)):
            x1, x2, y1, y2 = face_rect_list[i]
            w = x2 - x1
            h = y2 - y1
            xw1 = max(int(x1 - 0.4 * w), 0)
            yw1 = max(int(y1 - 0.4 * h), 0)
            xw2 = min(int(x2 + 0.4 * w), img_w - 1)
            yw2 = min(int(y2 + 0.4 * h), img_h - 1)

            faces[i, :, :, :] = cv2.resize(im[yw1:yw2 + 1, xw1:xw2 + 1, :], (self.img_size, self.img_size))

        results = self.model.predict(faces)
        predicted_genders = results[0]
        ages = np.arange(0, 101).reshape(101, 1)
        predicted_ages = results[1].dot(ages).flatten()

        ret_age_gender = []
        for i in range(len(face_rect_list)):
            ret_age_gender.append([int(predicted_ages[i]),
                                   "Female" if predicted_genders[i][0] > 0.5 else "Male"])

        return ret_age_gender

    def _recognize_face(self, frame, rect):
        shape = self.predictor(frame, rect)
        face_description = self.recognizer.compute_face_descriptor(frame, shape)
        return np.asarray(face_description)

    def get_feature128(self, frame, dets):

        descriptions = []

        for k, r in enumerate(dets):
            face_description = self._recognize_face(frame, r)
            descriptions.append(np.asarray(face_description))

        return descriptions

    def check_image(self, img, db_data=[], f_recog=False, f_age_gender=False, f_emotion=False):
        time1 = time.time()

        age_gender = []
        str_emotion_list = []
        name_list = []

        # -------------- face --------------
        rect, face_list, coordinate_list = self.get_face(img)

        # --------------- age_gender --------------
        if f_age_gender:
            age_gender = self.detect_age_gender_multi_face(img, face_list, coordinate_list)

        # --------------- emotion -----------------
        if f_emotion:
            landmark = self.get_landmarks(img, rect)
            for i in range(len(face_list)):
                feature = func.get_emotion_features(landmark[i])
                pred_y = self.new_clf.predict([feature])
                str_emotion = self.emotion_list[pred_y[0]]

                str_emotion_list.append(str_emotion)

        # --------------- similarity -----------------
        if f_recog:
            feature128_list = self.get_feature128(img, rect)

            for f_ind in range(len(face_list)):
                if len(db_data) == 0:
                    str_id = "0"
                    str_name = "fake"
                    str_match = "100.0"
                else:

                    max_match = 0.0
                    for row in db_data:
                        feature_row = row[5].split()

                        match_val = func.get_match_value(feature128_list[f_ind], feature_row)

                        if match_val > max_match:
                            max_match = match_val
                            max_row = row

                    str_id = str(max_row[0])
                    str_name = max_row[2]
                    str_match = str(max_match)

                name_list.append([str_id, str_name, str_match])

        print time.time() - time1

        return face_list, coordinate_list, name_list, age_gender, str_emotion_list

if __name__ == '__main__':
    class_face = FaceEngine()

    conn = sqlite3.connect('face_db.db')
    c = conn.cursor()
    c.execute('SELECT * FROM face_table')
    db_face = c.fetchall()

    image = cv2.imread("../img_src/1.jpg")

    print class_face.check_image(image, db_data=db_face, f_recog=True, f_age_gender=True, f_emotion=True)
    print class_face.check_image(image, db_data=db_face, f_recog=True, f_emotion=True)
