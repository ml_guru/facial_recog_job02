
import requests
import base64
import json


def req_img_enroll(face_image, face_name, ret_response, acc_id):

    img_file = open(face_image, 'rb')

    json_data = {
        "method": "enroll",
        "type": "image-base64",
        "data": base64.b64encode(img_file.read()).decode('UTF-8'),
        "name": face_name,
        "account": acc_id,
        "response": ret_response
    }

    return json_data


def req_img_check(face_image, ret_response, acc_id):
    img_file = open(face_image, 'rb')

    json_data = {
        "method": "check",
        "type": "image-base64",
        "data": base64.b64encode(img_file.read()).decode('UTF-8'),
        "account": acc_id,
        "response": ret_response
    }

    return json_data


def req_img_delete(face_id, acc_id):

    json_data = {
        "method": "delete",
        "type": "id",
        "account": acc_id,
        "data": face_id
    }

    return json_data


def req_img_format(acc_id):
    json_data = {
        "account": acc_id,
        "method": "format"
    }

    return json_data


def req_img_download(acc_id):
    json_data = {
        "account": acc_id,
        "method": "download"
    }

    return json_data


def req_video(video_info, ret_response, acc_id):

    video_file = open(video_info, 'rb')

    json_data = {
        "method": "check",
        "type": "video-base64",
        "data": base64.b64encode(video_file.read()).decode('UTF-8'),
        "account": acc_id,
        "response": ret_response
    }

    return json_data


def send_req(req_json):

    response = requests.post(url=url_server, json=req_json)
    print("Server responded with %s" % response.status_code)

    rep_json = response.json()['result']
    print json.dumps(rep_json, indent=4)

    if "data" in rep_json:
        if rep_json['type'] == 'image-base64':
            img_out = open('reply_image.jpg', 'wb')
        elif rep_json['type'] == 'video-base64':
            img_out = open('reply_video.avi', 'wb')
        else:
            img_out = open('face.db', 'wb')

        img_out.write(rep_json['data'].decode('base64'))
        img_out.close()


if __name__ == '__main__':

    # url_server = 'http://0.0.0.0:3000/api/face_server/v1.0'
    url_server = 'http://18.217.112.30:3000/api/face_server/v1.0'

    p_response = "detail"
    acc_id = 'account1'

    # json_req = req_img_enroll("d:/photo.png", "WBing", p_response, acc_id)
    json_req = req_img_check("img_src/11.jpg", p_response, acc_id)
    # json_req = req_img_delete(2, acc_id)
    # json_req = req_img_format(acc_id)
    # json_req = req_video("video_src/clips.mov", p_response, acc_id)
    # json_req = req_img_download(acc_id)

    # print json_req
    send_req(json_req)
